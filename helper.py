import random
from question_bank import questions

def play(total_questions):
    if total_questions == 0:
        response = input("Are you ready to play y/n:  ")
        if response == "y":
            print("Ok, lets go!")
            return True
        else:
            print("You should really practice...")
            exit()
    else:
        response = input("Carry on?; y/n\n  ")
        if response == "y":
            return True
        else:
            exit()

def game(total_questions, score):
    while play(total_questions) == True:
        for question in random.sample(questions, len(questions)):
            print(question["question"])
            print("a." + question["a"])
            print("b." + question["b"])
            print("c." + question["c"])
            print("d." + question["d"])

            user_answer = input("a, b, c, or d?")
            if user_answer.lower() == question["answer"]:
                print( "You got it!")
                score += 1
                total_questions += 1
                print(f"You've scored {score} out of {total_questions}.")
                questions.remove(question)
            else:
                print("That's incorrect. The correct answer is: ", question["explanation"])
                total_questions += 1
                print(f"You've scored {score} out of {total_questions}.")
                questions.remove(question)

            if len(questions) == 0:
                print("That's all folks!")
                exit()
            else:
                play(total_questions)
